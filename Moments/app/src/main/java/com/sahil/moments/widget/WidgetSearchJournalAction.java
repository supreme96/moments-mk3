package com.sahil.moments.widget;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;

import com.sahil.moments.R;
import com.sahil.moments.search.DatePickerFragment;

/**
 * Created by sahil on 31-Oct-16.
 */
public class WidgetSearchJournalAction extends Activity implements AuthenticationDialogFragment.AuthenticationInterface {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_init_activity);
        DialogFragment authenticationFragement = new AuthenticationDialogFragment();
        authenticationFragement.show(getFragmentManager(), "authenticationDialog");
    }

    @Override
    public void authenticationSuccess() {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getFragmentManager(), "datePicker");
    }
}
