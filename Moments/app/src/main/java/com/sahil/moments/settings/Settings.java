package com.sahil.moments.settings;


import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.support.v7.app.ActionBar;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.RingtonePreference;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MenuItem;
import android.widget.Toast;

import com.sahil.moments.R;
import com.sahil.moments.db.LogsDBContract;
import com.sahil.moments.notification.ReminderNotificationSetReceiver;

import java.util.List;

import static com.sahil.moments.R.string.pref_reminder_notify_enable_key;
import static com.sahil.moments.R.string.pref_reminder_time_key;

public class Settings extends AppCompatPreferenceActivity {

    PreferenceManager preferenceManager;
    SharedPreferences sharedPreferences;
    NotificationTimePicker notificationTimePickerPreference;
    SharedPreferences.Editor sharedPrefsEditor;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferenceManager = getPreferenceManager();
        preferenceManager.setSharedPreferencesName(getString(R.string.shared_preferences_key));
        preferenceManager.setSharedPreferencesMode(Context.MODE_PRIVATE);
        addPreferencesFromResource(R.xml.pref_general);
        notificationTimePickerPreference = (NotificationTimePicker) getPreferenceScreen().findPreference(getString(pref_reminder_time_key));
        checkNotifTimePickerEnable();
        EditTextPreference passwordEditTextPreference = (EditTextPreference) getPreferenceScreen()
                .findPreference(getString(R.string.login_shared_preference_password));

        passwordEditTextPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String changeValue = (String) newValue;
                if(changeValue.length()>=4 && changeValue.length()<=16){
                    preference.getEditor().putString(getString(R.string.login_shared_preference_password), changeValue);
                    Toast.makeText(Settings.this, getString(R.string.pref_password_changed), Toast.LENGTH_SHORT).show();
                    return true;
                }
                else{
                    Toast.makeText(Settings.this, getString(R.string.pref_password_not_changed), Toast.LENGTH_SHORT).show();
                    return false;
                }
            }
        });

        EditTextPreference usernameEditTextPreference = (EditTextPreference) getPreferenceScreen()
                .findPreference(getString(R.string.login_shared_preference_username));

        usernameEditTextPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String changeValue = (String) newValue;
                if(changeValue.length()>=3){
                    preference.getEditor().putString(getString(R.string.login_shared_preference_username), changeValue);
                    Toast.makeText(Settings.this, getString(R.string.pref_username_changed), Toast.LENGTH_SHORT).show();
                    return true;
                }
                else{
                    Toast.makeText(Settings.this, getString(R.string.pref_username_not_changed), Toast.LENGTH_SHORT).show();
                    return false;
                }
            }
        });

        CheckBoxPreference notifEnableCheckBoxPreference = (CheckBoxPreference) getPreferenceScreen()
                .findPreference(getString(pref_reminder_notify_enable_key));

        notifEnableCheckBoxPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                Boolean notifEnabled = (Boolean) newValue;
                if(notifEnabled == Boolean.TRUE){
                    //Toast.makeText(Settings.this, "Check enabled", Toast.LENGTH_SHORT).show();
                    sharedPreferences = preferenceManager.getSharedPreferences();
                    sharedPrefsEditor = sharedPreferences.edit();
                    sharedPrefsEditor.putBoolean(getString(R.string.notification_enabled_shared_preference), true);
                    boolean result = sharedPrefsEditor.commit();
                    if(result){
                        Toast.makeText(Settings.this, getString(R.string.notification_time_picker_instruction_toast), Toast.LENGTH_SHORT).show();
                        NotificationTimePicker timePicker = (NotificationTimePicker) getPreferenceScreen().findPreference(getString(pref_reminder_time_key));
                        timePicker.setEnabled(true);
                        timePicker.showDialog(null);
                    }
                }
                else{
                    //update the shared prefs for preventing notifications after boot up.
                    sharedPreferences = preferenceManager.getSharedPreferences();
                    sharedPrefsEditor = sharedPreferences.edit();
                    sharedPrefsEditor.putBoolean(getString(R.string.notification_enabled_shared_preference), false);
                    boolean result = sharedPrefsEditor.commit();
                    if(result){
                        //Canceling the repeating alarm to stop notifications in this session (before the next time the device is rebooted).
                        Intent intent = new Intent(Settings.this, ReminderNotificationSetReceiver.class);
                        PendingIntent alarmIntent = PendingIntent.getBroadcast(Settings.this, 0, intent, 0);
                        AlarmManager alarmMgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                        alarmMgr.cancel(alarmIntent);
                        //Repeating alarms cancelled.
                        //Disable the time picker perference option.
                        Toast.makeText(Settings.this, getString(R.string.notification_reminders_disabled_toast), Toast.LENGTH_SHORT).show();
                        NotificationTimePicker timePicker = (NotificationTimePicker) getPreferenceScreen().findPreference(getString(pref_reminder_time_key));
                        timePicker.setEnabled(false);
                    }
                }
                return true;
            }
        });
    }

    protected void checkNotifTimePickerEnable(){
        sharedPreferences = preferenceManager.getSharedPreferences();
        Boolean savedPreferenceValue = sharedPreferences.getBoolean(getString(pref_reminder_notify_enable_key), false);
        if(savedPreferenceValue.booleanValue() == false){
            notificationTimePickerPreference.setEnabled(false);
        }
        else{
            notificationTimePickerPreference.setEnabled(true);
        }
    }

}
