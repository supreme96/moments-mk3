package com.sahil.moments.editor;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;

import com.sahil.moments.R;

/**
 * Created by sahil on 31-Oct-16.
 */
public class DeleteConfirmationDialog extends DialogFragment {

    public interface JournalDeletionConfirmationInterface{
        void deletionConfirmed();
    }

    protected JournalDeletionConfirmationInterface deletionConfirmationCallback;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.confirm_delete_dialog_msg)
                .setPositiveButton(R.string.action_confirm_delete, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        deletionConfirmationCallback.deletionConfirmed();
                    }
                })
                .setNegativeButton(R.string.action_cancel_delete, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        DeleteConfirmationDialog.this.getDialog().cancel();
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            deletionConfirmationCallback = (JournalDeletionConfirmationInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement ImageActionsDialogCallback Interface");
        }
    }
}
