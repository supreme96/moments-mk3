# Moments
Udacity Android NanoDegree Capstone Project
<br />
Many people record a digital journal of their life on the Internet in form of blogs and vlogs, but not every detail of one’s life is meant to be put out in the public. There are many intimate details in our everyday life that we want to record but keep them only to ourselves. Today, smartphones are some of the most personal digital devices we use everyday and Moments is an app that will allow it’s users to maintain a personal journal regularly, one that is accessible to them anytime they want.
Also having a digital journal allows you to avoid the problem of making room for  those traditional personal journals in your closet.

#User Interface

![Alt text](/screenshots/01.png?raw=true "One time user registeration")
![Alt text](/screenshots/02.png?raw=true "features user authentication to app")
![Alt text](/screenshots/03.png?raw=true "main screen with journal entries of past")
![Alt text](/screenshots/04.png?raw=true "entries can be expanded to view full size")
![Alt text](/screenshots/05.png?raw=true "present day journal editor")
![Alt text](/screenshots/06.png?raw=true "attach upto 3 photos with your entries")
![Alt text](/screenshots/07.png?raw=true "select images from your gallery")
![Alt text](/screenshots/08.png?raw=true "entry can be deleted only on the day it was created")
![Alt text](/screenshots/09.png?raw=true "search for a day's entry by it's date")
![Alt text](/screenshots/10.png?raw=true "search results")
![Alt text](/screenshots/11.png?raw=true "Homescreen widget to quickly edit or search for an entry")
![Alt text](/screenshots/12.png?raw=true "widget too featuers password authentication")

#Intended User
Any one who has a habbit to writing a journal, wants to develop that habit, those who need a place to let out their feelings or for someone on the move who doesnot want to carry a heavy journal with them all the time.

#Features
●	Saves daily logs as text. <br />
●	Saves a few pictures with the journal in case of a special event. <br />
●	Allows to see all journals at once or search for a particuar one. <br />
●	Homescreen Widget for quick access. <br />
●	Password protection. <br />

#Libararies Used 
App uses open source libraries : <br />
<br />
1.Image Selector by Darshan Doarai -- https://github.com/darsh2/MultipleImageSelect <br />
2.Glide by BumpTech -- https://github.com/bumptech/glide <br />
 

