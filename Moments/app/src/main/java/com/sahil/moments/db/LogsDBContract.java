package com.sahil.moments.db;

import android.provider.BaseColumns;

/**
 * Created by sahil on 14-Oct-16.
 */
public class LogsDBContract implements BaseColumns{

    public static final class Log{
        public static final String TABLE_NAME = "logs";
        public static final String COLUMN_ID = "_id";
        public static final String COLUMN_NAME_LOG_TITLE = "title";
        public static final String COLUMN_NAME_DATE = "date";
        public static final String COLUMN_NAME_PATH_IMAGE_1 = "image1";
        public static final String COLUMN_NAME_PATH_IMAGE_2 = "image2";
        public static final String COLUMN_NAME_PATH_IMAGE_3 = "image3";
        public static final String COLUMN_NAME_LOG_CONTENT = "content";
    }

}















