package com.sahil.moments.editor;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;

import com.sahil.moments.R;

/**
 * Created by sahil on 23-Oct-16.
 */
public class ImageActionsDialog extends DialogFragment {

    protected ImageActionsDialogCallback mCallback;

    public interface ImageActionsDialogCallback{
        public void imageActionCallback(int position);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.image_action_dialog_title)
                .setItems(R.array.image_action_dialog_actions, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int position) {
                        mCallback.imageActionCallback(position);
                    }
                });
        return builder.create();
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (ImageActionsDialogCallback) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement ImageActionsDialogCallback Interface");
        }
    }
}
