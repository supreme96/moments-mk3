package com.sahil.moments.viewer;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SimpleCursorTreeAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.sahil.moments.R;
import com.sahil.moments.db.LogsContentProvider;
import com.sahil.moments.db.LogsDBContract;

import java.io.File;
import java.util.zip.Inflater;

/**
 * Created by sahil on 28-Oct-16.
 */
public class NewJournalAdapter extends SimpleCursorTreeAdapter {

    Context mContext;

    public NewJournalAdapter(Context context, Cursor cursor, int collapsedGroupLayout, int expandedGroupLayout, String[] groupFrom,
                             int[] groupTo, int childLayout, String[] childFrom, int[] childTo) {
        super(context, cursor, collapsedGroupLayout, expandedGroupLayout, groupFrom, groupTo, childLayout, childFrom, childTo);
        mContext = context;
    }

    @Override
    protected Cursor getChildrenCursor(Cursor cursor) {
        String id = cursor.getString(cursor.getColumnIndex(LogsDBContract.Log.COLUMN_ID));
        String selection = LogsDBContract.Log.COLUMN_ID + " = ?";
        String[] selectionArgs = {id};
        String[] projection = {LogsDBContract.Log.COLUMN_ID, LogsDBContract.Log.COLUMN_NAME_LOG_CONTENT};
        Cursor childCursor = mContext.getContentResolver().query(LogsContentProvider.CONTENT_URI
                , projection, selection, selectionArgs, null);
        return childCursor;
    }

    @Override
    protected void bindChildView(View view, Context context, Cursor cursor, boolean isLastChild) {
        String content = cursor.getString(cursor.getColumnIndex(LogsDBContract.Log.COLUMN_NAME_LOG_CONTENT));
        ((TextView) view.findViewById(R.id.elv_body_tv)).setText(content);
    }

    @Override
    protected void bindGroupView(View view, final Context context, Cursor cursor, boolean isExpanded) {
        FrameLayout collapsedParent = (FrameLayout)view.findViewById(R.id.elv_header_collapsed_fl);
        FrameLayout expandedParent = (FrameLayout) view.findViewById(R.id.elv_header_expanded_fl);
        FrameLayout container = (FrameLayout) view.findViewById(R.id.elb_header_container);
        FrameLayout.LayoutParams layoutParams;
        if(!isExpanded){
            //header is collapsed
            expandedParent.setVisibility(View.GONE);
            collapsedParent.setVisibility(View.VISIBLE);

            String date = cursor.getString(cursor.getColumnIndex(LogsDBContract.Log.COLUMN_NAME_DATE));
            String title = cursor.getString(cursor.getColumnIndex(LogsDBContract.Log.COLUMN_NAME_LOG_TITLE));
            String body = cursor.getString(cursor.getColumnIndex(LogsDBContract.Log.COLUMN_NAME_LOG_CONTENT));
            String imagePath;
            if (!cursor.getString(cursor.getColumnIndex(LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_1)).equals("")) {
                imagePath = cursor.getString(cursor.getColumnIndex(LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_1));
            }
            else if (!cursor.getString(cursor.getColumnIndex(LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_2)).equals("")) {
                imagePath = cursor.getString(cursor.getColumnIndex(LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_2));
            }
            else if (!cursor.getString(cursor.getColumnIndex(LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_3)).equals("")) {
                imagePath = cursor.getString(cursor.getColumnIndex(LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_3));
            }
            else{
                imagePath = "noImage";
            }
            TextView dateview = (TextView) view.findViewById(R.id.elv_header_tv_date);
            dateview.setText(date);
            TextView titleview = (TextView) view.findViewById(R.id.elv_header_tv_title);
            if(title != null) {
                titleview.setText(title);
            }
            else{
                titleview.setText(body);
            }
            ImageView imageView = (ImageView) view.findViewById(R.id.elv_header_im);
            if(imagePath.equals("noImage")){
                imageView.setImageDrawable(context.getDrawable(R.mipmap.moments_icon));
            }
            else{
                File imgFile1 = new File(imagePath);
                if (imgFile1.exists()) {
                    Glide.with(context).load(imgFile1).into(imageView);
                } else {
                    Toast.makeText(context, context.getString(R.string.error_image_not_found), Toast.LENGTH_SHORT).show();
                }
            }
        }
        else{
            //header is expanded
            collapsedParent.setVisibility(View.GONE);
            expandedParent.setVisibility(View.VISIBLE);

            String date = cursor.getString(cursor.getColumnIndex(LogsDBContract.Log.COLUMN_NAME_DATE));
            String title = cursor.getString(cursor.getColumnIndex(LogsDBContract.Log.COLUMN_NAME_LOG_TITLE));
            String[] imagePaths = new String[3];
            int count = 0;
            if (!cursor.getString(cursor.getColumnIndex(LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_1)).equals("")) {
                imagePaths[count] = cursor.getString(cursor.getColumnIndex(LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_1));
                count++;
            }
            if (!cursor.getString(cursor.getColumnIndex(LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_2)).equals("")) {
                imagePaths[count] = cursor.getString(cursor.getColumnIndex(LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_2));
                count++;
            }
            if (!cursor.getString(cursor.getColumnIndex(LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_3)).equals("")) {
                imagePaths[count] = cursor.getString(cursor.getColumnIndex(LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_3));
                count++;
            }
            TextView titleview = (TextView) view.findViewById(R.id.elv_header_tv_title_expanded);
            if(title != null) {
                titleview.setText(title);
            }
            TextView dateview = (TextView) view.findViewById(R.id.elv_header_tv_date_expanded);
            dateview.setText(date);
            ImageView[] imageViews = new ImageView[3];
            imageViews[0] = (ImageView) view.findViewById(R.id.elv_header_im1);
            imageViews[1] = (ImageView) view.findViewById(R.id.elv_header_im2);
            imageViews[2] = (ImageView) view.findViewById(R.id.elv_header_im3);
            if(count != 0 ){
                File imgFile1;
                for(int i=0; i<count; i++){
                    imgFile1 = new File(imagePaths[i]);
                    if (imgFile1.exists()) {
                        final int j =i;
                        Glide.with(context).load(imgFile1).into(imageViews[i]);
                        final String imagePath = imagePaths[i];
                        imageViews[i].setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setDataAndType(Uri.parse("file://" + imagePath), "image/*");
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                mContext.getApplicationContext().startActivity(intent);
                            }
                        });
                    } else {
                        Toast.makeText(context, context.getString(R.string.error_image_not_found), Toast.LENGTH_SHORT).show();
                    }
                }
            }
            else{
                ((LinearLayout) view.findViewById(R.id.elv_expanded_header_img_ll)).setVisibility(View.GONE);
            }
        }
    }
}
