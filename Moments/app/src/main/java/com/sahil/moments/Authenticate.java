package com.sahil.moments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.TwitterAuthProvider;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

public class  Authenticate extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    int RC_SIGN_IN = 1107;
    private FirebaseAuth firebaseAuth;

    private CallbackManager mCallbackManager;
    TwitterLoginButton twitterLoginButton;

    private static String TAG = "Authenticate";

    public TextView statusTV;

    //Authenticate activity code.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Twitter core initialized start
        TwitterConfig twitterConfig = new TwitterConfig.Builder(this)
                .logger(new DefaultLogger(Log.DEBUG))
                .twitterAuthConfig(new TwitterAuthConfig(
                        getString(R.string.twitter_app_id), getString(R.string.twitter_app_secret)))
                .debug(true)
                .build();
        Twitter.initialize(twitterConfig);
        //Twitter core initialized end

        setContentView(R.layout.activity_authenticate);

        statusTV = (TextView) findViewById(R.id.authentication_status_tv);
        firebaseAuth = FirebaseAuth.getInstance();

        //Initializing twitter.

        twitterLoginButton = (TwitterLoginButton) findViewById(R.id.login_button_twitter);
        twitterLoginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                Log.d(TAG, "twitterLogin:success" + result);
                handleTwitterSession(result.data);
            }

            @Override
            public void failure(TwitterException exception) {
                Log.d(TAG, "twitterLogin:failure", exception);
                //updateUI(null);
            }
        });

        //Initialization data for google sign in.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile()
                .requestIdToken(getString(R.string.default_web_client_id))
                .build();

        final GoogleApiClient mGoogleApiClient = new GoogleApiClient.Builder(Authenticate.this)
                .enableAutoManage(Authenticate.this, Authenticate.this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        //End of google's initialization.

        //Google signin button
        Button google_button = (Button) findViewById(R.id.login_button_google);
        google_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(intent,RC_SIGN_IN);
            }
        });


        // Initialize Facebook Login button

        mCallbackManager = CallbackManager.Factory.create();
        LoginButton loginButton = (LoginButton) findViewById(R.id.login_button_fb);
        loginButton.setReadPermissions("email", "public_profile");
        loginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook:onSuccess:" + loginResult);
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
                // ...
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);

            }
        });

    }

    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);

        final ProgressDialog progressDialog = ProgressDialog.show(Authenticate.this, "Signing In", "Please Wait ...", true);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Toast.makeText(Authenticate.this, "signInWithCredential:success", Toast.LENGTH_SHORT).show();
                            FirebaseUser user = firebaseAuth.getCurrentUser();
                            statusTV.setText(user.getUid());

                            moveToMainScreen();
                            //update user interface here according to failed authentication here.
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(Authenticate.this, "Authentication failed.",Toast.LENGTH_SHORT).show();

                            if(task.getException() instanceof FirebaseAuthUserCollisionException){
                                Toast.makeText(Authenticate.this, "Account with same email but different Provider exists", Toast.LENGTH_SHORT).show();
                            }
                            //user interface must be updated here according to failed authentication
                        }
                        progressDialog.dismiss();
                    }
                });
    }

    private void handleTwitterSession(TwitterSession session) {
        Log.d(TAG, "handleTwitterSession:" + session);

        final ProgressDialog progressDialog = ProgressDialog.show(Authenticate.this, "Signing In", "Please Wait ...", true);

        AuthCredential credential = TwitterAuthProvider.getCredential(
                session.getAuthToken().token,
                session.getAuthToken().secret);

        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = firebaseAuth.getCurrentUser();
                            //statusTV.setText(user.getUid());
                            Log.d(TAG, "USER DISPLAY NAME " + user.getDisplayName());
                            moveToMainScreen();
                            //update user interface here according to failed authentication here.
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.d(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(Authenticate.this, "Authentication failed.",Toast.LENGTH_SHORT).show();

                            if(task.getException() instanceof FirebaseAuthUserCollisionException){
                                Toast.makeText(Authenticate.this, "Account with same email but different Provider exists", Toast.LENGTH_SHORT).show();
                            }
                            //user interface must be updated here according to failed authentication
                        }
                        progressDialog.dismiss();
                    }
                });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == RC_SIGN_IN){

            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            //handleSignInResult(result);
            if (result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                //statusTV.setText(account.getDisplayName());
                Log.d(getClass().getSimpleName(), "Log in success");
                firebaseAuthWithGoogle(account);

            } else {
                Log.d(getClass().getSimpleName(), "Log in failure" + result.getStatus());
            }

        }
        else {
            // Pass the activity result back to the Facebook SDK
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
            //Pass the activity result back to the twitter SDK
            twitterLoginButton.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getIdToken());

        // [START_EXCLUDE silent]
        final ProgressDialog progressDialog = ProgressDialog.show(Authenticate.this, "Signing In", "Please Wait ...", true);
        // [END_EXCLUDE]

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = firebaseAuth.getCurrentUser();
                            moveToMainScreen();
                            //updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            /*Toast.makeText(Authenticate.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();*/

                            if(task.getException() instanceof FirebaseAuthUserCollisionException){
                                statusTV.setText("An account with same email but different Provider exists already. " +
                                        "Please use the same login method you used to login before");
                                statusTV.setTextColor(getResources().getColor(R.color.black));
                            }
                            //updateUI(null);
                        }

                        // [START_EXCLUDE]
                        progressDialog.dismiss();
                        // [END_EXCLUDE]
                    }
                });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(getClass().getSimpleName(), "Failed to establish google api client connection");
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser user = firebaseAuth.getCurrentUser();
        if(user != null){
            Toast.makeText(this, "Signed In Already!", Toast.LENGTH_SHORT).show();
            moveToMainScreen();
        }
        else{
            statusTV.setText("Please SignIn Using One of the Method");
            statusTV.setTextColor(getResources().getColor(R.color.black));
        }
    }

    public void moveToMainScreen(){
        Intent intent = new Intent(Authenticate.this, Main.class);
        startActivity(intent);
        finish();
    }

}
