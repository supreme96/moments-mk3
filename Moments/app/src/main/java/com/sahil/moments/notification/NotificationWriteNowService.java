package com.sahil.moments.notification;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;

import com.sahil.moments.Login;

/**
 * Created by sahil on 26-11-2016.
 */
public class NotificationWriteNowService extends IntentService {

    public NotificationWriteNowService(String name) {
        super(name);
    }

    public NotificationWriteNowService() {
        super("moments_notification");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.cancel(ReminderNotificationSetReceiver.Notification_id);
        Intent write_now_intent = new Intent(getApplicationContext(), Login.class);
        write_now_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(write_now_intent);
    }
}
