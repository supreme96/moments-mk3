package com.sahil.moments.settings;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.widget.TimePicker;
import android.widget.Toast;

import com.sahil.moments.R;
import com.sahil.moments.notification.ReminderNotificationSetReceiver;

import java.util.Calendar;

/**
 * Created by sahil on 24-11-2016.
 */
public class NotificationTimePicker extends DialogPreference {

    private int setHour, setMinutes;
    private Calendar currentCalendar;
    private final Context mContext;
    private TimePickerDialog timePickerDialog;
    private SharedPreferences.Editor sharedPrefsEditor;


    public NotificationTimePicker(final Context context, AttributeSet attrs) {
        super(context, attrs);
        currentCalendar = Calendar.getInstance(); //Calendar with current time to be passed to the time picker dialog for its intial value when displayed to the user.
        mContext = context;
        timePickerDialog = new TimePickerDialog(mContext, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                //Make a calendar instance with values of time picked by the user in the time picker dialog.
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(System.currentTimeMillis());
                calendar.set(Calendar.HOUR_OF_DAY, hour);
                calendar.set(Calendar.MINUTE, minute);
                //Instance created
                //Update the set values in the shared preferences.
                sharedPrefsEditor = getSharedPreferences().edit();
                sharedPrefsEditor.putInt(context.getString(R.string.notification_remind_minute_shared_preference), minute);
                sharedPrefsEditor.putInt(context.getString(R.string.notification_remind_hour_shared_preference), hour);
                boolean result = sharedPrefsEditor.commit();
                if (result == true){
                    //Shared prefs successfully updated
                    //Toast.makeText(context, "Will remind you at " + hour + ":" + minute, Toast.LENGTH_SHORT).show();
                    //Settings the repeating alarm manager to invoke the notification creater every 24 hrs.
                    AlarmManager alarmMgr ;
                    Intent intent = new Intent(mContext, ReminderNotificationSetReceiver.class);
                    PendingIntent alarmIntent = PendingIntent.getBroadcast(mContext, 0, intent, 0);
                    alarmMgr = (AlarmManager)mContext.getSystemService(Context.ALARM_SERVICE);
                    alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                            AlarmManager.INTERVAL_DAY, alarmIntent);
                    Toast.makeText(mContext, mContext.getString(R.string.notification_time_picker_reminder_set_success), Toast.LENGTH_SHORT).show();
                }else{
                    //Some error occured preventing updation of the shared prefs.
                    Toast.makeText(mContext, mContext.getString(R.string.notification_time_picker_reminder_set_failure), Toast.LENGTH_SHORT).show();
                }
            }
        }, currentCalendar.get(Calendar.HOUR_OF_DAY), currentCalendar.get(Calendar.MINUTE), false);
    }

    @Override
    protected void showDialog(Bundle state) {
        timePickerDialog.show();
    }
}

