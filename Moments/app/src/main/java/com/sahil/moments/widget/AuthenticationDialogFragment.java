package com.sahil.moments.widget;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.sahil.moments.Main;
import com.sahil.moments.R;
import com.sahil.moments.editor.ImageActionsDialog;

/**
 * Created by sahil on 31-Oct-16.
 */
public class AuthenticationDialogFragment extends DialogFragment {

    private SharedPreferences sharedPreferences;
    private String spRetrievedUsername;
    private String spRetrievedPassword;
    private EditText passwordEditText;
    private EditText usernameEditText;
    private AuthenticationInterface mAuthenticationCallback;

    public interface AuthenticationInterface{
        void authenticationSuccess();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);


        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mAuthenticationCallback = (AuthenticationInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement ImageActionsDialogCallback Interface");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_widget_authentication, null);

        usernameEditText = (EditText) view.findViewById(R.id.authentication_dialog_username_et);
        passwordEditText = (EditText) view.findViewById(R.id.authentication_dialog_password_et);

        sharedPreferences = getActivity().getSharedPreferences(getString(R.string.shared_preferences_key), Context.MODE_PRIVATE);
        spRetrievedUsername = sharedPreferences.getString(getString(R.string.login_shared_preference_username), getString(R.string.app_authentication_default_values));
        spRetrievedPassword = sharedPreferences.getString(getString(R.string.login_shared_preference_password), getString(R.string.app_authentication_default_values));



        if(!spRetrievedUsername.equals(getString(R.string.app_authentication_default_values))){
            usernameEditText.setText(spRetrievedUsername);
            usernameEditText.setSelection(spRetrievedUsername.length());
        }
        else{
            Log.e("sahil authentication", "couldn't retrieve username");
        }

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(view)
                .setTitle(getString(R.string.authentication_dialog_title))
                // Add action buttons
                .setPositiveButton(R.string.widget_btn_sign_in, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        if(usernameEditText.getText().length()>=3 && (passwordEditText.getText().length()>=4 && passwordEditText.getText().length()<=16)) {
                            String enteredPassword = passwordEditText.getText().toString();
                            String enteredUsername = usernameEditText.getText().toString();
                            if(enteredUsername.equals(spRetrievedUsername) && enteredPassword.equals(spRetrievedPassword)){
                                mAuthenticationCallback.authenticationSuccess();
                            }
                            else{
                                authenticationFailed();
                                usernameEditText.setError(getString(R.string.edit_text_authentication_failed_error));
                                passwordEditText.setError(getString(R.string.edit_text_authentication_failed_error));
                            }
                        }else{
                            invalidInputError();
                            usernameEditText.setError(getString(R.string.edit_text_invalid_input_error));
                            passwordEditText.setError(getString(R.string.edit_text_invalid_input_error));
                        }
                    }
                })
                .setNegativeButton(R.string.authentication_dialog_btn_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        AuthenticationDialogFragment.this.getDialog().cancel();
                    }
                });
        return builder.create();
    }

    public void invalidInputError(){
        Toast.makeText(getActivity(), getString(R.string.toast_invalid_input), Toast.LENGTH_SHORT).show();
    }

    public void authenticationFailed(){
        Toast.makeText(getActivity(), getString(R.string.toast_authentication_failed), Toast.LENGTH_SHORT).show();
    }

}
