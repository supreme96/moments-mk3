package com.sahil.moments;

import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.sahil.moments.db.LogsContentProvider;
import com.sahil.moments.db.LogsDBContract;
import com.sahil.moments.editor.editor;
import com.sahil.moments.search.DatePickerFragment;
import com.sahil.moments.settings.Settings;
import com.sahil.moments.viewer.NewJournalAdapter;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Main extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>{

    InterstitialAd interstitialAd;
    private Tracker mTracker;
    private static final String screenName = "Journal_Entries_Viewer_Screen";
    //Below adUnitID is for test ads. The one below it is for live apps.
    // private static final String adUnitID = "ca-app-pub-3940256099942544/1033173712";
    private static final String adUnitID = "ca-app-pub-3990828485740374/3898540049";

    private static String LOG_TAG = Main.class.getSimpleName();
    private ExpandableListView expandableListView;

    public static boolean recordExists = false;

    public static SimpleDateFormat idDateFormat = new SimpleDateFormat("yyyyMMdd");
    private static Date presentDate = new Date();

    private static final int JOURNALS_LOADER = 1107;
    private String[] projection = {
            LogsDBContract.Log.COLUMN_ID,
            LogsDBContract.Log.COLUMN_NAME_DATE,
            LogsDBContract.Log.COLUMN_NAME_LOG_TITLE,
            LogsDBContract.Log.COLUMN_NAME_LOG_CONTENT,
            LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_1,
            LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_2,
            LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_3,
    };
    private NewJournalAdapter newJournalAdapter;

    @Override
    public void onBackPressed() {
        Toast.makeText(Main.this, getString(R.string.log_out_prompt), Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.main_screen_fab);

        checkExistingRecord(getApplicationContext());

        if(recordExists){
            fab.setImageDrawable(getDrawable(R.drawable.ic_mode_edit_white_48dp));
        }
        else{
            fab.setImageDrawable(getDrawable(R.drawable.ic_note_add_white_48dp));
        }
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Main.this, editor.class);
                startActivity(intent);
            }
        });

/*
        mTracker.setScreenName(screenName);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
*/

        super.onResume();
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                //The below line is used to specify that the device on which the ad is running is a test device.
                // .addTestDevice("SEE_YOUR_LOGCAT_TO_GET_YOUR_DEVICE_ID")
                .build();

        interstitialAd.loadAd(adRequest);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        expandableListView = (ExpandableListView) findViewById(R.id.main_scrn_content_expandable_list_view);
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                if (expandableListView.isGroupExpanded(i)){
                    expandableListView.collapseGroup(i);
                }
                else{
                    expandableListView.expandGroup(i);
                }
                return true;
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.main_screen_toolbar);
        setSupportActionBar(toolbar);

        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(adUnitID);
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                Toast.makeText(Main.this, getString(R.string.logout_toast_message), Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        requestNewInterstitial();

/*
        AnalyticsApplication analyticsApplication = (AnalyticsApplication) getApplication();
        mTracker = analyticsApplication.getDefaultTracker();
*/

        getSupportLoaderManager().initLoader(JOURNALS_LOADER, null, this);
    }

    public static void checkExistingRecord(Context context){
        String date = idDateFormat.format(presentDate);
        String[] projection = {
                LogsDBContract.Log.COLUMN_ID,
                LogsDBContract.Log.COLUMN_NAME_DATE,
                LogsDBContract.Log.COLUMN_NAME_LOG_TITLE,
                LogsDBContract.Log.COLUMN_NAME_LOG_CONTENT
        };

        String selection = LogsDBContract.Log.COLUMN_ID + " = ?";
        String[] selectionArgs = {date};

        Cursor cursor = context.getContentResolver().query(LogsContentProvider.CONTENT_URI, projection, selection, selectionArgs, null);
        if(cursor.getCount() == 0){
            recordExists = false;
        }else {
            recordExists = true;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {

            case R.id.menu_action_search_journal:
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getFragmentManager(), "datePicker");
                break;

            case R.id.menu_action_settings:
                startActivity(new Intent(Main.this, Settings.class));
                break;

            case R.id.menu_action_log_out:

                if (interstitialAd.isLoaded()) {
                    interstitialAd.show();
                } else {
                    Toast.makeText(this, getString(R.string.logout_toast_message), Toast.LENGTH_SHORT).show();
                    finish();
                }
                break;

            default:
                Log.e(LOG_TAG, "Invalid Main Activity Options Item Selected");


        }

        return true;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if(id == JOURNALS_LOADER){
            return new CursorLoader(this,
                    LogsContentProvider.CONTENT_URI, projection, null, null, LogsDBContract.Log.COLUMN_ID + " DESC");
        }
        else{
            return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if(data.getCount()>0){
            data.moveToFirst();
        }
        else{
            Toast.makeText(Main.this, getString(R.string.toast_no_existing_journals_found),Toast.LENGTH_SHORT).show();
        }
        newJournalAdapter = new NewJournalAdapter(getApplicationContext(), data, R.layout.elv_header_expanded, R.layout.elv_header_expanded,
                new String[]{LogsDBContract.Log.COLUMN_NAME_LOG_TITLE, LogsDBContract.Log.COLUMN_NAME_DATE}, new int[]{R.id.elv_header_tv_title, R.id.elv_header_tv_date, R.id.elv_header_im1, R.id.elv_header_im2, R.id.elv_header_im3, R.id.elv_header_im},
                R.layout.elv_body, new String[]{LogsDBContract.Log.COLUMN_NAME_LOG_CONTENT},
                new int[]{R.id.elv_body_tv});
        expandableListView.setAdapter(newJournalAdapter);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }
}











