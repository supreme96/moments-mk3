package com.sahil.moments.db;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by sahil on 16-Oct-16.
 */
public class LogsContentProvider extends ContentProvider {

    private static final String Authority = "com.sahil.moments.db.provider";
    private static final String table_name = "logs";
    private static final String content_url = "content://" + Authority + "/" + table_name;
    public static final Uri CONTENT_URI = Uri.parse(content_url);

    private LogsDBHelper logsDBHelper;
    private static final String LOG_TAG = LogsContentProvider.class.getSimpleName();

    @Override
    public boolean onCreate() {
        logsDBHelper = new LogsDBHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        if (uri.equals(CONTENT_URI)){
            SQLiteDatabase db = logsDBHelper.getWritableDatabase();
            Cursor cursor = db.query(LogsDBContract.Log.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
            return cursor;
        }
        else{
            Log.e(LOG_TAG, "Couldn't complete query. Uri mismatch");
            return null;


        }
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        if (uri.equals(CONTENT_URI)){
            SQLiteDatabase db = logsDBHelper.getWritableDatabase();
            long id = db.insert(LogsDBContract.Log.TABLE_NAME, null, contentValues);
            db.close();
            getContext().getContentResolver().notifyChange(uri, null);
            return ContentUris.withAppendedId(uri, id);
        }
        else{
            Log.e(LOG_TAG, "Couldn't insert. Uri mismatch");
            return null;
        }
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        if (uri.equals(CONTENT_URI)){
            SQLiteDatabase db = logsDBHelper.getWritableDatabase();
            int num = db.delete(LogsDBContract.Log.TABLE_NAME, selection,selectionArgs);
            getContext().getContentResolver().notifyChange(uri, null);
            return num;
        }
        else{
            Log.e(LOG_TAG, "Couldn't delete. Uri mismatch");
            return -1;
        }
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String selection, String[] selectionArgs) {
        if (uri.equals(CONTENT_URI)){
            SQLiteDatabase db = logsDBHelper.getWritableDatabase();
            int num = db.update(LogsDBContract.Log.TABLE_NAME, contentValues, selection, selectionArgs);
            getContext().getContentResolver().notifyChange(uri, null);
            return num;
        }
        else{
            Log.e(LOG_TAG, "Couldn't update. Uri mismatch");
            return -1;
        }
    }
}
