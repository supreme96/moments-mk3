package com.sahil.moments.editor;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.darsh.multipleimageselect.activities.AlbumSelectActivity;
import com.darsh.multipleimageselect.helpers.Constants;
import com.darsh.multipleimageselect.models.Image;
import com.sahil.moments.R;
import com.sahil.moments.db.LogsContentProvider;
import com.sahil.moments.db.LogsDBContract;
import com.sahil.moments.editor.ImageActionsDialog.ImageActionsDialogCallback;
import com.sahil.moments.settings.Settings;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class editor extends AppCompatActivity implements ImageActionsDialogCallback, DeleteConfirmationDialog.JournalDeletionConfirmationInterface{

    public static final String LOG_TAG = editor.class.getSimpleName();

    public CollapsingToolbarLayout editorCollapsingToolbar;
    public EditText editorBodyEditText;
    public EditText editorTitleEditText;
    public ImageView editorImgView1;
    public ImageView editorImgView2;
    public ImageView editorImgView3;
    public FloatingActionButton saveFab;

    private static boolean recordExists = false;

    private static final int VIEW_PHOTO_ACTION_CHOSEN = 0;
    private static final int REMOVE_PHOTO_ACTION_CHOSEN = 1;
    private static final int ANOTHER_PHOTO_PICK_ACTION_CHOSEN = 2;

    private String previousRecordTitle;
    private String previousRecordBody;
    private String previousRecordDate;
    private String previousRecordId;

    private String[] editorImagePaths;

    private ImageView currentProcessingImageView;
    private int currentProcessingImageViewIndex;
    private int[] usedImageViews = {0, 0, 0};
    private static int ENTRY_IMAGE_NUMBER_LIMIT = 1;

    private SimpleDateFormat toolbarDateFormat = new SimpleDateFormat("EEE, MMM dd", Locale.getDefault());
    private SimpleDateFormat idDateFormat = new SimpleDateFormat("yyyyMMdd");
    private SimpleDateFormat recordDateFormat = new SimpleDateFormat("dd MMM, yyyy", Locale.getDefault());
    private Date presentDate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);
        Toolbar toolbar = (Toolbar) findViewById(R.id.editor_screen_toolbar);
        setSupportActionBar(toolbar);

        saveFab = (FloatingActionButton) findViewById(R.id.save_fab);


        presentDate = new Date();

        editorBodyEditText = (EditText) findViewById(R.id.editor_body_edit_text);
        editorTitleEditText = (EditText) findViewById(R.id.editor_title_edit_text);

        editorCollapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.editor_toolbar_layout);
        editorCollapsingToolbar.setTitle(toolbarDateFormat.format(presentDate));

        editorImgView1 = (ImageView) findViewById(R.id.editor_img_view_1);
        editorImgView2 = (ImageView) findViewById(R.id.editor_img_view_2);
        editorImgView3 = (ImageView) findViewById(R.id.editor_img_view_3);

        editorImagePaths = new String[3];

        checkExistingRecord();
        setSaveFabAction(recordExists);
        if(recordExists) {
            loadExistingRecordInEditor();
        }
    }

    protected void setSaveFabAction(boolean recordExistsValue){
        if(recordExistsValue){
            saveFab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    updateLog(previousRecordId);
                }
            });
        }
        else{
            saveFab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    saveLog();
                }
            });
        }
    }

    public void editorImageViewClicked(View view){
        int i = -1;
        switch(view.getId()){
            case R.id.editor_img_view_1:
                i = 1;
                break;
            case R.id.editor_img_view_2:
                i = 2;
                break;
            case R.id.editor_img_view_3:
                i = 3;
                break;
        }
        currentProcessingImageViewIndex = i-1;
        currentProcessingImageView = (ImageView) view;
        if(usedImageViews[currentProcessingImageViewIndex] == 1 ){
            //View already has a image
            ImageActionsDialog imageActionsDialog = new ImageActionsDialog();
            imageActionsDialog.show(getFragmentManager(), "imageActionsDialog");
        }
        else {
            addImageToEntry(view);
        }
    }

    public void removeImageFromEntry(View view){
        ((ImageView) view).setImageDrawable(getDrawable(R.drawable.ic_add_a_photo_white_48dp));
        ((ImageView) view).setScaleType(ImageView.ScaleType.CENTER);
        usedImageViews[currentProcessingImageViewIndex] = 0;
        editorImagePaths[currentProcessingImageViewIndex] = "";
    }

    public void addImageToEntry(View view){
        Intent intent = new Intent(this, AlbumSelectActivity.class);
        //set limit on number of images that can be selected
        intent.putExtra(Constants.INTENT_EXTRA_LIMIT, ENTRY_IMAGE_NUMBER_LIMIT);
        startActivityForResult(intent, Constants.REQUEST_CODE);
        currentProcessingImageView = (ImageView) view;
        usedImageViews[currentProcessingImageViewIndex] = 1;
    }

    protected void loadImageIntoView(String path){
        if(!path.contentEquals("")) {
            File imgFile1 = new File(path);
            if (imgFile1.exists()) {
                currentProcessingImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                Glide.with(getApplicationContext()).load(imgFile1).into(currentProcessingImageView);
            } else {
                Toast.makeText(getApplicationContext(), getString(R.string.error_image_not_found), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.REQUEST_CODE && resultCode == RESULT_OK && data != null) {
            //The array list has the image paths of the selected images
            ArrayList<Image> images = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);

            if(images.size()!=0) {
                editorImagePaths[currentProcessingImageViewIndex] = images.get(0).path;
                loadImageIntoView(editorImagePaths[currentProcessingImageViewIndex]);
            }
            else{
                usedImageViews[currentProcessingImageViewIndex] = 0;
                Toast.makeText(getApplicationContext(), getString(R.string.error_image_not_added), Toast.LENGTH_SHORT).show();
            }
        }
        else{
            usedImageViews[currentProcessingImageViewIndex] = 0;
        }
    }

    protected void checkExistingRecord(){
        String date = idDateFormat.format(presentDate);
        String[] projection = {
                LogsDBContract.Log.COLUMN_ID,
                LogsDBContract.Log.COLUMN_NAME_DATE,
                LogsDBContract.Log.COLUMN_NAME_LOG_TITLE,
                LogsDBContract.Log.COLUMN_NAME_LOG_CONTENT,
                LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_1,
                LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_2,
                LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_3,
        };

        String selection = LogsDBContract.Log.COLUMN_ID + " = ?";
        String[] selectionArgs = {date};

        Cursor cursor = getContentResolver().query(LogsContentProvider.CONTENT_URI, projection, selection, selectionArgs, null);
        if(cursor!=null) {
            if (cursor.getCount() == 0) {
                recordExists = false;
            } else {
                recordExists = true;
                cursor.moveToFirst();
                previousRecordId = cursor.getString(cursor.getColumnIndex(LogsDBContract.Log.COLUMN_ID));
                previousRecordDate = cursor.getString(cursor.getColumnIndex(LogsDBContract.Log.COLUMN_NAME_DATE));
                previousRecordTitle = cursor.getString(cursor.getColumnIndex(LogsDBContract.Log.COLUMN_NAME_LOG_TITLE));
                previousRecordBody = cursor.getString(cursor.getColumnIndex(LogsDBContract.Log.COLUMN_NAME_LOG_CONTENT));
                if (cursor.getString(cursor.getColumnIndex(LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_1)) != null) {
                    currentProcessingImageViewIndex=0;
                    editorImagePaths[currentProcessingImageViewIndex] = cursor.getString(cursor.getColumnIndex(LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_1));

                }
                if (cursor.getBlob(cursor.getColumnIndex(LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_2)) != null) {
                    currentProcessingImageViewIndex=1;
                    editorImagePaths[currentProcessingImageViewIndex] = cursor.getString(cursor.getColumnIndex(LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_2));
                }
                if (cursor.getBlob(cursor.getColumnIndex(LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_3)) != null) {
                    currentProcessingImageViewIndex=2;
                    editorImagePaths[currentProcessingImageViewIndex] = cursor.getString(cursor.getColumnIndex(LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_3));
                }
            }
            cursor.close();
        }
        else{
            Log.e(LOG_TAG, "Prev Record Check Failed. Null Cursor");
        }
    }

    protected void loadExistingRecordInEditor(){
        if(previousRecordTitle!=null){
            editorTitleEditText.setText(previousRecordTitle);
            editorTitleEditText.setSelection(previousRecordTitle.length());
        }
        editorBodyEditText.setText(previousRecordBody);
        editorBodyEditText.setSelection(previousRecordBody.length());
        editorTitleEditText.clearFocus();
        if(editorImagePaths[0] != null) {
            if ((!editorImagePaths[0].isEmpty()) && (!editorImagePaths[0].contentEquals(""))){
                currentProcessingImageViewIndex=0;
                usedImageViews[currentProcessingImageViewIndex] = 1;
                currentProcessingImageView = editorImgView1;
                loadImageIntoView(editorImagePaths[currentProcessingImageViewIndex]);
            }
        }
        if(editorImagePaths[1] != null) {
            if ((!editorImagePaths[1].isEmpty()) && (!editorImagePaths[1].contentEquals(""))){
                currentProcessingImageViewIndex=1;
                usedImageViews[currentProcessingImageViewIndex] = 1;
                currentProcessingImageView = editorImgView2;
                loadImageIntoView(editorImagePaths[currentProcessingImageViewIndex]);
            }
        }
        if(editorImagePaths[2] != null) {
            if ((!editorImagePaths[2].isEmpty()) && (!editorImagePaths[2].contentEquals(""))){
                currentProcessingImageViewIndex=2;
                usedImageViews[currentProcessingImageViewIndex] = 1;
                currentProcessingImageView = editorImgView3;
                loadImageIntoView(editorImagePaths[currentProcessingImageViewIndex]);
            }
        }
    }

    protected void saveLog(){
        String date = idDateFormat.format(presentDate);
        ContentValues values = new ContentValues();
        values.put(LogsDBContract.Log.COLUMN_ID, date);
        values.put(LogsDBContract.Log.COLUMN_NAME_DATE, recordDateFormat.format(presentDate));
        if(!TextUtils.isEmpty(editorTitleEditText.getText().toString())) {
            values.put(LogsDBContract.Log.COLUMN_NAME_LOG_TITLE, editorTitleEditText.getText().toString());
        }
        if(!TextUtils.isEmpty(editorBodyEditText.getText().toString())) {
            values.put(LogsDBContract.Log.COLUMN_NAME_LOG_CONTENT, editorBodyEditText.getText().toString());
        }
        else{
            Toast.makeText(this, getString(R.string.empty_body_toast), Toast.LENGTH_SHORT).show();
        }
        if(usedImageViews[0] == 1){
            values.put(LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_1, editorImagePaths[0]);
        }
        else{
            values.put(LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_1, "");
        }
        if(usedImageViews[1] == 1){
            values.put(LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_2, editorImagePaths[1]);
        }
        else{
            values.put(LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_2, "");
        }
        if(usedImageViews[2] == 1){
            values.put(LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_3, editorImagePaths[2]);
        }
        else{
            values.put(LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_3, "");
        }
        Uri uri = getContentResolver().insert(LogsContentProvider.CONTENT_URI, values);
        Toast.makeText(editor.this, getString(R.string.editor_toast_save_successful), Toast.LENGTH_SHORT).show();
        recordExists = true;
        setSaveFabAction(recordExists);
    }

    protected void updateLog(String date){
        ContentValues values = new ContentValues();
        values.put(LogsDBContract.Log.COLUMN_ID, date);
        values.put(LogsDBContract.Log.COLUMN_NAME_DATE, recordDateFormat.format(presentDate));
        if(!TextUtils.isEmpty(editorTitleEditText.getText().toString())) {
            values.put(LogsDBContract.Log.COLUMN_NAME_LOG_TITLE, editorTitleEditText.getText().toString());
        }
        if(!TextUtils.isEmpty(editorBodyEditText.getText().toString())) {
            values.put(LogsDBContract.Log.COLUMN_NAME_LOG_CONTENT, editorBodyEditText.getText().toString());
        }
        else{
            Toast.makeText(this, getString(R.string.empty_body_toast), Toast.LENGTH_SHORT).show();
        }
        if(usedImageViews[0] == 1){
            values.put(LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_1, editorImagePaths[0]);
        }
        else {
            values.put(LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_1, "");
        }
        if(usedImageViews[1] == 1){
            values.put(LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_2, editorImagePaths[1]);
        }
        else{
            values.put(LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_2, "");
        }
        if(usedImageViews[2] == 1){
            values.put(LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_3, editorImagePaths[2]);
        }
        else{
            values.put(LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_3, "");
        }
        String selection = LogsDBContract.Log.COLUMN_ID + " = ?";
        String[] selectionArgs = {date};

        int count = getContentResolver().update(LogsContentProvider.CONTENT_URI, values, selection, selectionArgs);
        Toast.makeText(editor.this, getString(R.string.editor_toast_update_successful), Toast.LENGTH_SHORT).show();
        recordExists = true;
        setSaveFabAction(recordExists);
    }

    protected void deleteLog(){
        String date = idDateFormat.format(presentDate);
        String selection = LogsDBContract.Log.COLUMN_ID + " = ? ";
        String[] selectionArgs = {date};
        int count = getContentResolver().delete(LogsContentProvider.CONTENT_URI, selection, selectionArgs);
        recordExists = false;
        setSaveFabAction(recordExists);
        editorTitleEditText.setText("");
        editorBodyEditText.setText("");
        editorTitleEditText.clearFocus();
        removeImageFromEntry(editorImgView1);
        removeImageFromEntry(editorImgView2);
        removeImageFromEntry(editorImgView3);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
         super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_editor, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        int id = item.getItemId();

        switch (id) {
            case R.id.menu_action_delete_log:
                DialogFragment deleteConfirmationDialog = new DeleteConfirmationDialog();
                deleteConfirmationDialog.show(getFragmentManager(), "deleteConfirmation");
                break;

            case R.id.menu_action_settings:
                startActivity(new Intent(editor.this, Settings.class));
                break;

            default:
                Log.e(LOG_TAG, "Invalid Main Activity Options Item Selected");
        }
        return true;
    }

    @Override
    public void imageActionCallback(int position) {
        switch(position){
            case VIEW_PHOTO_ACTION_CHOSEN:
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.parse("file://" +
                            editorImagePaths[currentProcessingImageViewIndex]), "image/*");
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                break;

            case REMOVE_PHOTO_ACTION_CHOSEN:
                    removeImageFromEntry(currentProcessingImageView);
                break;

            case ANOTHER_PHOTO_PICK_ACTION_CHOSEN:
                    addImageToEntry(currentProcessingImageView);
                break;

            default:
                    Log.e(LOG_TAG, "Editor Image Actions Dialog Invalid Action");
                break;
        }

    }

    @Override
    public void deletionConfirmed() {
        deleteLog();
    }
}
