package com.sahil.moments.search;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.Toast;

import com.sahil.moments.Main;
import com.sahil.moments.db.LogsContentProvider;
import com.sahil.moments.db.LogsDBContract;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by sahil on 08-Jul-16.
 */
public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    Context mContext;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        mContext = getActivity().getApplicationContext();

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        // Do something with the date chosen by the user

        Date date = new Date();

        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

        try {
            //Below i have done month+1 because dont know why datepicker is returning value that is one month less.
            date = format.parse(Integer.toString(day) + "-" + Integer.toString(month + 1) + "-" + Integer.toString(year));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String formattedDate = Main.idDateFormat.format(date);

        //Toast.makeText(getActivity(), "date formatted is :" + formattedDate, Toast.LENGTH_SHORT).show();

        JournalSearchTask task = new JournalSearchTask();
        task.execute(formattedDate);

    }


    class JournalSearchTask extends AsyncTask<String, Void, Boolean>{

        private String[] projection = {
                LogsDBContract.Log.COLUMN_NAME_DATE,
                LogsDBContract.Log.COLUMN_NAME_LOG_TITLE,
                LogsDBContract.Log.COLUMN_NAME_LOG_CONTENT,
                LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_1,
                LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_2,
                LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_3,
        };
        String selection = LogsDBContract.Log.COLUMN_ID + " = ?";

        boolean recordExists;
        String recordDate;
        String recordTitle;
        String recordContent;
        String[] recordImagePaths = new String[3];

        @Override
        protected Boolean doInBackground(String... strings) {
            String searchDate = strings[0];
            Cursor cursor = mContext.getContentResolver().query(LogsContentProvider.CONTENT_URI, projection, selection, new String[]{searchDate}, null);
            if(cursor.getCount() == 0){
                recordExists = false;
            }
            else{
                recordExists = true;
                cursor.moveToFirst();
                recordDate = cursor.getString(cursor.getColumnIndex(LogsDBContract.Log.COLUMN_NAME_DATE));
                recordTitle = cursor.getString(cursor.getColumnIndex(LogsDBContract.Log.COLUMN_NAME_LOG_TITLE));
                recordContent = cursor.getString(cursor.getColumnIndex(LogsDBContract.Log.COLUMN_NAME_LOG_CONTENT));
                recordImagePaths[0] = cursor.getString(cursor.getColumnIndex(LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_1));
                recordImagePaths[1] = cursor.getString(cursor.getColumnIndex(LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_2));
                recordImagePaths[2] = cursor.getString(cursor.getColumnIndex(LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_3));
            }
            return Boolean.valueOf(recordExists);
        }

        @Override
        protected void onPostExecute(Boolean recordExistenceResult) {
            super.onPostExecute(recordExistenceResult);
            Intent intent = new Intent(mContext, SearchViewer.class);
            intent.putExtra(SearchViewer.INTENT_EXTRA_RECORD_EXIST_TOKEN, recordExistenceResult);
            if(recordExistenceResult.booleanValue()){
                intent.putExtra(SearchViewer.INTENT_EXTRA_DATE_TOKEN, recordDate);
                intent.putExtra(SearchViewer.INTENT_EXTRA_TITLE_TOKEN, recordTitle);
                intent.putExtra(SearchViewer.INTENT_EXTRA_CONTENT_TOKEN, recordContent);
                intent.putExtra(SearchViewer.INTENT_EXTRA_IMAGE_PATHS_TOKEN, recordImagePaths);
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(intent);
        }
    }
}

