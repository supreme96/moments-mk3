package com.sahil.moments.notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.NotificationCompat;

import com.sahil.moments.Login;
import com.sahil.moments.R;

/**
 * Created by sahil on 24-11-2016.
 */
public class ReminderNotificationSetReceiver extends BroadcastReceiver {

    public static final int Notification_id = 1911;
    public static final int NOTIFICATION_STATUS_RECEIVER_REQ_CODE = 11071996;
    public static final int NOTIFICATION_ACTION_DISMISS = 01;

    CharSequence action_yes;
    CharSequence action_no;

    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationCompat.Builder notif = new NotificationCompat.Builder(context);
        NotificationManager nm = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
        action_yes = context.getString(R.string.notification_action_written);
        action_no = context.getString(R.string.notification_action_write_now);

        Intent already_written_intent = new Intent(context, NotificationDismissReceiver.class);
        PendingIntent already_written_pending_intent = PendingIntent.getBroadcast(context, NOTIFICATION_STATUS_RECEIVER_REQ_CODE, already_written_intent, 0);
        NotificationCompat.Action action_already_written = new NotificationCompat.Action.Builder(0,action_yes, already_written_pending_intent).build();

        Intent lets_write_intent = new Intent(context, NotificationWriteNowService.class);
        PendingIntent lets_write_pending_intent = PendingIntent.getService(context, 0, lets_write_intent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Action action_lets_write = new NotificationCompat.Action.Builder(0, action_no, lets_write_pending_intent).build();

        Intent notification_clicked_intent = new Intent(context, Login.class);
        PendingIntent notification_clicked_pending_intent = PendingIntent.getActivity(context, 0, notification_clicked_intent, PendingIntent.FLAG_UPDATE_CURRENT);

        notif.setWhen(System.currentTimeMillis());
        notif.setContentTitle(context.getString(R.string.notification_title));
        notif.setSmallIcon(R.drawable.ic_stat_moments_notification_icon);
        notif.setContentText(context.getString(R.string.notification_content_question));
        notif.addAction(action_already_written);

        notif.addAction(action_lets_write);
        notif.setContentIntent(notification_clicked_pending_intent);
        notif.setAutoCancel(true);

        nm.notify(Notification_id, notif.build());
    }
}