package com.sahil.moments.notification;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.sahil.moments.R;

import java.util.Calendar;

/**
 * Created by sahil on 26-12-2016.
 */

public class DeviceBootUpBroadcastReceiver extends BroadcastReceiver {

    private boolean notificationReminderEnabled;
    private int notificationTimeHour;
    private int notificationTimeMinute;
    private SharedPreferences sharedPreferences;

    @Override
    public void onReceive(Context context, Intent intent) {
        sharedPreferences = context.getSharedPreferences(context.getString(R.string.shared_preferences_key), Context.MODE_PRIVATE);
        notificationReminderEnabled = sharedPreferences.getBoolean(context.getString(R.string.notification_enabled_shared_preference), false);
        if(notificationReminderEnabled){
            notificationTimeHour = sharedPreferences.getInt(context.getString(R.string.notification_remind_hour_shared_preference), -1);
            notificationTimeMinute = sharedPreferences.getInt(context.getString(R.string.notification_remind_minute_shared_preference), -1);
            if((notificationTimeHour != -1)&&(notificationTimeMinute !=  -1)){
                Calendar calendar = getCalendarForReminderNotification();
                setReminderNotification(context, calendar);
            }
            else {
                //create a notification or dialog for this.
                Toast.makeText(context, context.getString(R.string.notification_boot_up_reinforce_failure), Toast.LENGTH_LONG).show();
            }
        }
    }

    private Calendar getCalendarForReminderNotification(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, notificationTimeHour);
        calendar.set(Calendar.MINUTE, notificationTimeMinute);
        return calendar;
    }

    private void setReminderNotification(Context context, Calendar calendar){
        AlarmManager alarmMgr ;
        Intent reminderIntent = new Intent(context, ReminderNotificationSetReceiver.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, 0, reminderIntent, 0);
        alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, alarmIntent);
    }
}
