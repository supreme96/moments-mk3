package com.sahil.moments.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.sahil.moments.R;
import com.sahil.moments.db.LogsDBContract;

import java.util.zip.Inflater;

/**
 * Created by sahil on 31-Oct-16.
 */
public class MomentsWidgetProvider extends AppWidgetProvider {

    private static final int INTENT_REQ_CODE = 101;
    private static final int INTENT_NO_FLAGS = 0;

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        final int N = appWidgetIds.length;

        // Perform this loop procedure for each App Widget that belongs to this provider
        for (int i=0; i<N; i++) {
            int appWidgetId = appWidgetIds[i];

            // Get the layout for the App Widget and attach an on-click listener
            // to the button

            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget);
            Intent searchIntent = new Intent(context, WidgetSearchJournalAction.class);
            views.setOnClickPendingIntent(R.id.widget_search_journal_btn, PendingIntent.getActivity(
                    context, INTENT_REQ_CODE, searchIntent, INTENT_NO_FLAGS));

            Intent writeIntent = new Intent(context, WidgetWriteJournalAction.class);
            views.setOnClickPendingIntent(R.id.widget_write_journal_btn, PendingIntent.getActivity(
                    context, INTENT_REQ_CODE, writeIntent, INTENT_NO_FLAGS));

            // Tell the AppWidgetManager to perform an update on the current app widget
            appWidgetManager.updateAppWidget(appWidgetId, views);
        }
    }
}
