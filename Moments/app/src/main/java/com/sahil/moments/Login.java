package com.sahil.moments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class Login extends AppCompatActivity {

    private String enteredUsername;
    private String enteredPassword;
    private String spRetrievedUsername;
    private String spRetrievedPassword;
    private EditText passwordEditText;
    private EditText usernameEditText;
    private SharedPreferences sharedPreferences;
    private static String FIRST_LAUNCH = "isFirstLaunch";
    private boolean isFirstLaunch;
    private Button loginSubmitButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = getSharedPreferences(getString(R.string.shared_preferences_key), Context.MODE_PRIVATE);
        isFirstLaunch = sharedPreferences.getBoolean(FIRST_LAUNCH, true);

        setContentView(R.layout.activity_login);
        loginSubmitButton = (Button) findViewById(R.id.login_submit_button);

        if(isFirstLaunch){
            loginSubmitButton.setText(getString(R.string.action_create_user));
            initCreateUser();
        }
        else{
            loginSubmitButton.setText(getString(R.string.action_sign_in_short));
            initUserSignIn();
        }
    }

    public void initUserSignIn(){
        usernameEditText = (EditText) findViewById(R.id.login_username_edit_text);
        passwordEditText = (EditText) findViewById(R.id.login_password_edit_text);
        loginSubmitButton = (Button) findViewById(R.id.login_submit_button);
        spRetrievedUsername = sharedPreferences.getString(getString(R.string.login_shared_preference_username), getString(R.string.app_authentication_default_values));
        spRetrievedPassword = sharedPreferences.getString(getString(R.string.login_shared_preference_password), getString(R.string.app_authentication_default_values));
        if(!spRetrievedUsername.equals(getString(R.string.app_authentication_default_values))){
            usernameEditText.setText(spRetrievedUsername);
            usernameEditText.setSelection(spRetrievedUsername.length());
        }
        loginSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(usernameEditText.getText().length()>=3 && (passwordEditText.getText().length()>=4 && passwordEditText.getText().length()<=16)) {
                    enteredPassword = passwordEditText.getText().toString();
                    enteredUsername = usernameEditText.getText().toString();
                    if(enteredUsername.equals(spRetrievedUsername) && enteredPassword.equals(spRetrievedPassword)){
                        Toast.makeText(getApplicationContext(), getString(R.string.toast_welcome_back) + " " + spRetrievedUsername, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(Login.this, Main.class);
                        startActivity(intent);
                    }
                    else{
                        authenticationFailed();
                        usernameEditText.setError(getString(R.string.edit_text_authentication_failed_error));
                        passwordEditText.setError(getString(R.string.edit_text_authentication_failed_error));
                    }
                }else{
                    invalidInputError();
                    usernameEditText.setError(getString(R.string.edit_text_invalid_input_error));
                    passwordEditText.setError(getString(R.string.edit_text_invalid_input_error));
                }
            }
        });
    }

    public void initCreateUser(){
        usernameEditText = (EditText) findViewById(R.id.login_username_edit_text);
        passwordEditText = (EditText) findViewById(R.id.login_password_edit_text);
        loginSubmitButton = (Button) findViewById(R.id.login_submit_button);
        loginSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(usernameEditText.getText().length()>=3 && (passwordEditText.getText().length()>=4 && passwordEditText.getText().length()<=16)) {
                    enteredUsername = usernameEditText.getText().toString();
                    enteredPassword = passwordEditText.getText().toString();
                    SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
                    sharedPreferencesEditor.putString(getString(R.string.login_shared_preference_username), enteredUsername);
                    sharedPreferencesEditor.putString(getString(R.string.login_shared_preference_password), enteredPassword);
                    sharedPreferencesEditor.putBoolean(FIRST_LAUNCH, false);
                    boolean result = sharedPreferencesEditor.commit();
                    if(result){
                        Toast.makeText(getApplicationContext(), getString(R.string.user_register_success), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(Login.this, Main.class);
                        startActivity(intent);
                    }
                    else{
                        Toast.makeText(getApplicationContext(), getString(R.string.user_register_failure), Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    invalidInputError();
                    usernameEditText.setError(getString(R.string.edit_text_invalid_input_error));
                    passwordEditText.setError(getString(R.string.edit_text_invalid_input_error));
                }
            }
        });
    }

    public void invalidInputError(){
        Toast.makeText(getApplicationContext(), getString(R.string.toast_invalid_input), Toast.LENGTH_SHORT).show();
    }

    public void authenticationFailed(){
        Toast.makeText(getApplicationContext(), getString(R.string.toast_authentication_failed), Toast.LENGTH_SHORT).show();
    }

    public void togglePasswordVisibility(View view){
        passwordEditText = (EditText) findViewById(R.id.login_password_edit_text);
        if(passwordEditText.getInputType() != InputType.TYPE_TEXT_VARIATION_PASSWORD){
            passwordEditText.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
            passwordEditText.setSelection(passwordEditText.getText().length());
        }
    }
}