package com.sahil.moments.search;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.sahil.moments.R;

import org.w3c.dom.Text;

import java.io.File;

public class SearchViewer extends AppCompatActivity {

    private static final String LOG_TAG = SearchViewer.class.getSimpleName();

    protected static final String INTENT_EXTRA_TITLE_TOKEN = "title";
    protected static final String INTENT_EXTRA_DATE_TOKEN = "date";
    protected static final String INTENT_EXTRA_CONTENT_TOKEN = "content";
    protected static final String INTENT_EXTRA_RECORD_EXIST_TOKEN = "record_exists";
    protected static final String INTENT_EXTRA_IMAGE_PATHS_TOKEN = "image_paths";

    private Boolean recordExist;
    private String recordTitle;
    private String recordDate;
    private String recordContent;
    private String[] recordImagePaths;

    public ImageView noResultsImageView;
    public TextView titleTextView;
    public TextView dateTextView;
    public TextView contentTextView;
    public ImageView[] imageViews = new ImageView[3];
    public RelativeLayout resultsRL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_viewer);

        Toolbar toolbar = (Toolbar) findViewById(R.id.search_viewer_screen_toolbar);
        setSupportActionBar(toolbar);

        noResultsImageView = (ImageView) findViewById(R.id.no_results_image_view);
        titleTextView = (TextView) findViewById(R.id.search_viewer_tv_title);
        dateTextView = (TextView) findViewById(R.id.search_viewer_tv_date);
        contentTextView = (TextView) findViewById(R.id.search_viewer_body_tv);
        imageViews[0] = (ImageView) findViewById(R.id.search_viewer_im1);
        imageViews[1] = (ImageView) findViewById(R.id.search_viewer_im2);
        imageViews[2] = (ImageView) findViewById(R.id.search_viewer_im3);
        resultsRL = (RelativeLayout) findViewById(R.id.search_viewer_results_rl);

        recordExist = getIntent().getBooleanExtra(INTENT_EXTRA_RECORD_EXIST_TOKEN, false);
        if(recordExist.booleanValue()){
            noResultsImageView.setVisibility(View.GONE);
            resultsRL.setVisibility(View.VISIBLE);

            recordDate = getIntent().getStringExtra(INTENT_EXTRA_DATE_TOKEN);
            recordTitle = getIntent().getStringExtra(INTENT_EXTRA_TITLE_TOKEN);
            recordContent = getIntent().getStringExtra(INTENT_EXTRA_CONTENT_TOKEN);
            recordImagePaths = getIntent().getStringArrayExtra(INTENT_EXTRA_IMAGE_PATHS_TOKEN);

            if(recordTitle!=null){
                titleTextView.setText(recordTitle);
                titleTextView.setEnabled(false);
                dateTextView.setText(recordDate);
                dateTextView.setEnabled(false);
            }
            else{
                titleTextView.setText(recordDate);
                titleTextView.setEnabled(false);
                dateTextView.setVisibility(View.GONE);
            }

            if(recordContent!=null && !recordContent.isEmpty()){
                contentTextView.setText(recordContent);
                contentTextView.setEnabled(false);
            }
            else{
                Log.e(LOG_TAG, "Search viewer encountered null or empty entry");
            }
            for(int i=0; i<3; i++){
                if(!recordImagePaths[i].equals("")){
                    File imgFile = new File(recordImagePaths[i]);
                    if (imgFile.exists()) {
                        Glide.with(getApplicationContext()).load(imgFile).into(imageViews[i]);
                        final String imagePath = recordImagePaths[i];
                        imageViews[i].setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setDataAndType(Uri.parse("file://" + imagePath), "image/*");
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });
                    } else {
                        imageViews[i].setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), getString(R.string.error_image_not_found), Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    imageViews[i].setVisibility(View.GONE);
                }
            }

        }
        else{
            noResultsImageView.setVisibility(View.VISIBLE);
            resultsRL.setVisibility(View.GONE);
        }
    }
}
