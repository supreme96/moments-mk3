package com.sahil.moments.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by sahil on 16-Oct-16.
 */

public class LogsDBHelper extends SQLiteOpenHelper {

    private String CMD_CREATE_TABLE = "CREATE TABLE " + LogsDBContract.Log.TABLE_NAME + " (" +
            LogsDBContract.Log.COLUMN_ID + " INTEGER PRIMARY KEY, "+
            LogsDBContract.Log.COLUMN_NAME_DATE + " TEXT NOT NULL, " +
            LogsDBContract.Log.COLUMN_NAME_LOG_CONTENT + " TEXT NOT NULL, " +
            LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_1 + " TEXT, " +
            LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_2 + " TEXT, " +
            LogsDBContract.Log.COLUMN_NAME_PATH_IMAGE_3 + " TEXT, " +
            LogsDBContract.Log.COLUMN_NAME_LOG_TITLE + " TEXT, " +
            "UNIQUE (" + LogsDBContract.Log.COLUMN_ID + ") ON CONFLICT REPLACE );";

    private static String dbName = "moments_journal.db";

    private static int dbVersion = 1;

    public LogsDBHelper(Context context) {
        super(context, dbName, null, dbVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CMD_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
    }
}
